<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\student;
use App\Models\Teacher;
 use App\Jobs\StudentCsv;
 use Bus;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
     
   public function index(Request $request) {

      
       return view('upload-file');
   }

   public function show(Request $request) {
        
    $data=Teacher::paginate(15);
      
return view('studentdata',compact(array('data')));
}



    
        
        public function store(Request $request)
    {
        $file = $request->file('file')->store('import');
       

        $import = new student;
        $import->import($file);

        if ($import->failures()->isNotEmpty()) {
            return back()->withFailures($import->failures());
        }


        return back()->withStatus('Import in queue, we will send notification after import finished.');
    
           
        
    }
     


//    public function batch() {

//     $batchid->request('id');
     
//      return Bus::findBatch($batchid);
//    }

   
//  public function batchInProgress()
//     {
//         $batches = DB::table('job_batches')->where('pending_jobs', '>', 0)->get();
//         if (count($batches) > 0) {
//             return Bus::findBatch($batches[0]->id);
//         }

//         return [];
//     }

    
}
