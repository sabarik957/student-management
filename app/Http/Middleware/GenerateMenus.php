<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        \Menu::make('MyNavBar', function ($menu) {
            $menu->add('Manage Product');
            $menu->add('Manage Role', 'manage role');
            $menu->add('Manage User', 'manage user');
            
        });

        return $next($request);
    }
}
