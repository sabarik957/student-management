<?php
namespace App\Imports;

use App\Models\Teacher;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Validators\Failure;
use Throwable;

class student implements
    ToCollection,
    WithHeadingRow,
    SkipsOnError,
    WithValidation,
    SkipsOnFailure,
    WithChunkReading,
    ShouldQueue,
    WithEvents
{
    use Importable, SkipsErrors, SkipsFailures, RegistersEventListeners;


    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $user = Teacher::create([     
               'student_name' => $row['student_name'],
                'email' => $row['email'],
                'age' =>$row['age'],
                'gender' => $row['gender'],
                'school' => $row['school'],
                'residential_address' => $row['residential_address'],
                'city' => $row['city'],
                'district' =>  $row['district'],
                'country' => $row['country'],
                'nationality' => $row['nationality'],
                'father_name' =>$row['father_name'],
            ]);

            
        }
    }

    public function rules(): array
    {
        return [
            '*.email' => ['email', 'unique:student,email']
        ];
    }


    public function chunkSize(): int
    {
        return 1000;
    }

    // public static function afterImport(AfterImport $event)
    // {
    // }

    // public function onFailure(Failure ...$failure)
    // {
    // }
}