<?php

use Illuminate\Database\Seeder;
use Database\Seeds\StudentTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([StudentTableSeeder::class,]);
    }
}
