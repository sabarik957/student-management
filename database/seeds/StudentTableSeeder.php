<?php

namespace Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  

        $faker = Faker::create();
        foreach(range(1,100000) as $index) {
            $gender = $faker->randomElement(['male', 'female','others']);
            DB::table('student')->insert([
                'student_name' => $faker->name($gender),
                'email' => $faker->unique()->safeEmail,
                'age' => $faker->numberBetween(25, 50),
                'gender' => $gender,
                'school' => $faker->streetName,
                'residential_address' => $faker->streetAddress,
                'city' => $faker->city,
                'district' => $faker->city                                ,
                'country' => $faker->country,
                'nationality' => $faker->country,
                'father_name' => $faker->name($gender),
    
            ]);

        } 

        
    }
}
