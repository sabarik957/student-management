<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    
    <link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet">
    
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>  
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

</head>
<body>
<div>
        Toggle column: <a class="toggle-vis" data-column="0">Student Name</a> - <a class="toggle-vis" data-column="1">Email</a> - <a class="toggle-vis" data-column="2">School</a> - <a class="toggle-vis" data-column="3">Age</a> - <a class="toggle-vis" data-column="4">Gender</a> - <a class="toggle-vis" data-column="5">Address</a>- <a class="toggle-vis" data-column="6">City</a>-<a class="toggle-vis" data-column="7">District</a>-<a class="toggle-vis" data-column="8">Country</a>-<a class="toggle-vis" data-column="9">Nationality</a>-<a class="toggle-vis" data-column="10">Father Name</a>
    </div>
    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Student Name</th>
                <th>Email</th>
                <th>School</th>
                <th>Age</th>
                <th>Gender</th>
                <th>Residential Address</th>
                <th>City</th>
                <th>District</th>
                <th>Country</th>
                <th>Nationality</th>
                <th>Father Name</th>
               
            </tr>
        </thead>
        <tbody>
           @foreach($data as $item)
          
            <tr>
                 
                <td>{{$item->student_name ?? ''}}</td>
                <td>{{$item->email ?? ''}}</td>
                <td>{{$item->school ?? ''}}</td>
                <td>{{$item->age ?? ''}}</td>
                <td>{{$item->gender ?? ''}}</td>
                <td>{{$item->residential_address ?? ''}}</td>
                <td>{{$item->city ?? ''}}</td>
                <td>{{$item->district ?? ''}}</td>
                <td>{{$item->country ?? ''}}</td>
                <td>{{$item->nationality ?? ''}}</td>
                <td>{{$item->father_name ?? ''}}</td>
            </tr>
           @endforeach

        
        </tbody>
       
    </table>
    {{ $data->links() }}
    
    <script type="text/javascript">
       $(document).ready(function() {
    var table = $('#example').DataTable( {
        "scrollY": "200px",
        "paging": false
    } );
 
    $('a.toggle-vis').on( 'click', function (e) {
        e.preventDefault();
 
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
 
        // Toggle the visibility
        column.visible( ! column.visible() );
    } );
} );
    </script>
</body>
</html>