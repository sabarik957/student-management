<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
  
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::resource('users', UserController::class);

Route::get('login/{provider}', 'SocialController@redirect');
Route::get('login/{provider}/callback','SocialController@Callback');

Route::get('/upload/index','StudentController@index')->name('import');
Route::post('/upload','StudentController@store');

Route::get('/show','StudentController@show')->name('show');
        

